$(document).ready(function () {
    $("#btn1").click(function () {
        $('#loaddiv').empty();
    });
    $("#btn2").click(function () {
        callPage();
    });
});

function callPage() {
    $('#loaddiv').html("<div id='regDiv' class='container' style='padding:20px 0'>" +
        "<div class='row'>" +
        "<div class='col-md-4'></div>" +
        "<div class='col-md-4'>" +
        "<div class='container'>" +
        "<div class='row'>" +
        "<div class='col-md-4'>" +

        "<div id='legend'>" +
        "<legend>Enter your details:</legend>" +
        "</div>" +
        "<div class='control-group'>" +
        "<label class='control-label' for='firstName'>First Name: </label>" +
        "<div class='controls'>" +
        "<input type='text' id='firstName' name='firstName' class='form-control input-lg'>" +
        "</div>" +
        "</div>" +
        "<div class='control-group'>" +
        "<label class='control-label' for='middleName'>MiddleName: </label>" +
        "<div class='controls'>" +
        "<input type='text' id='middleName' name='middleName' class='form-control input-lg'>" +
        "</div>" +
        "</div>" +
        "<div class='control-group'>" +
        "<label class='control-label' for='lastName'>Last Name: </label>" +
        "<div class='controls'>" +
        "<input type='text' id='lastName' name='lastName' class='form-control input-lg'>" +
        "</div>" +
        "</div>" +
        "<div class='control-group'>" +
        "<label class='control-label' for='loginReg'>Login:</label>"
        + "<div class='controls'>" +
        "<input type='text' id='loginReg' name='loginReg' class='form-control input-lg'>"
        + "</div>" +
        "</div>" +
        "<div class='control-group'>"
        + "<label class='control-label' for='passwordReg'>Password:</label>" +
        "<div class='controls'>" +
        "<input type='password' id='passwordReg' name='passwordReg' class='form-control input-lg'>" +
        "</div>" +
        "</div>" +
        "<div class='control-group'>" +
        "<div class='controls'>" +
        "<button class='btn btn-success btn-block' onclick=registration()>Sing up</button>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-4'></div>" +
        "</div>" +
        "</div>");
}

function registration() {
    var json = {
        firstName: $('#firstName').val(),
        middleName: $('#middleName').val(),
        lastName: $('#lastName').val(),
        login: $('#loginReg').val(),
        password: $('#passwordReg').val()
    };
    $.post({
        dataType: 'json',
        contentType: "application/json",
        url: '/registration/ajax',
        data: JSON.stringify(json),
        success: function (json) {
            alert('ok');
        }
    }).done(function (response) {
        alert("send registration data");
    });
    alert("Вы успешно зарегистрированы");
    $('#loaddiv').empty();
}